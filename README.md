# Vanilla JS Accordion

A simple and lightweight JS Accordion.

## Install

Copy the vendor-training-accordion-js.js file into your filesystem, and then include it in your website.

### How to use it

1. Write the markup

    Simple accordion:

        :::html
        <ul data-role="accordion">
            <li data-role="accordion-group">
                <ul data-role="accordion-section">
                    <li data-role="accordion-section-title">
                        <a data-role="title-toggle">
                            My First Heading
                        </a>
                    </li>
                    <li data-role="accordion-section-content">
                        Some Content here
                    </li>
                </ul>
            </li>
            <li data-role="accordion-group">
                <ul data-role="accordion-section">
                    <li data-role="accordion-section-title">
                        <a data-role="title-toggle">
                            My Second Heading
                        </a>
                    </li>
                    <li data-role="accordion-section-content">
                        Some Content here
                    </li>
                </ul>
            </li>
        </ul>
        
2. Config your accordion: If you want some panel to be opened by default (all panels will be closed by default), just add the "toggle-active" class to the section title.
  
        :::html
        <li data-role="accordion-group">
            <ul data-role="accordion-section">
                <li data-role="accordion-section-title" class="toggle-active">
                    <a data-role="title-toggle">
                        My Second Heading
                    </a>
                </li>
                <li data-role="accordion-section-content">
                    Some Content here
                </li>
            </ul>
        </li>  

3. Initialize your accordion

        :::javascript
        var accordion = new Accordion();
        



4. Find the demo provided. It also includes the styling in scss format for your convenience.