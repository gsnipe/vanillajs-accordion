/*!
 * Vanilla JS Accordion
 */

(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory;
    } else {
        root.Accordion = factory();
    }
})(this, function() {

    "use strict";

    var defaults = {
        selector: "[data-role='accordion']"
    };

    function Accordion(selector) {
        this.options = defaults;

        //Setting custom selector
        if (selector) { this.options.selector = selector }

        //Init the accordion
        this.init();
    }

    Accordion.prototype = {

        init: function() {

            this.options.accordions = document.querySelectorAll(this.options.selector);
            var Accordion = this;
            var accordionsLength = this.options.accordions.length;

            //Loop over the accordions
            for (var i = 0; i < accordionsLength; i++) {

                var titles = this.options.accordions[i].querySelectorAll("[data-role='accordion-section-title']"),
                    titlesLength = titles.length;

                //Loop over the accordion sections
                for (var j = 0; j < titlesLength; j++) {

                    var titleNode = titles[j],
                        titleNodeHasClass = titleNode.hasAttribute('class'),
                        depth = i.toString(); // Accordion index

                    // Set the depth. If there is a nested accordion then the depth value will be different for it.
                    titleNode.setAttribute("data-depth", depth);

                    // If toggle-active class is present then the tab is not going to be closed
                    if (!titleNodeHasClass) {
                        Accordion.hidePanel(titleNode)
                    }

                    //Set onclick event listener
                    titleNode.onclick = (function() {

                        var accordion = titles,
                            accordionDepth = depth;

                        return function() {

                            var currentPanel = this;

                            accordion.forEach(function(panel) {

                                if (panel !== currentPanel) {

                                    // If this panel is opened, then close it. depth attribute must match.
                                    if (panel.getAttribute("class") === "toggle-active" && panel.getAttribute("data-depth") === accordionDepth) {
                                        Accordion.hidePanel(panel);
                                    }

                                } else {

                                    if (currentPanel.getAttribute("class") === "toggle") {
                                        Accordion.showPanel(currentPanel);
                                    } else {
                                        Accordion.hidePanel(currentPanel);
                                    }

                                }

                            });

                        };

                    })();

                }

            }
        },

        hidePanel: function(panel) {
            panel.setAttribute("class", "toggle");
            panel.nextElementSibling.setAttribute("class", "content-closed");
        },

        showPanel: function(panel) {
            panel.setAttribute("class", "toggle-active");
            panel.nextElementSibling.setAttribute("class", "");
        }

    };

    return Accordion;

});